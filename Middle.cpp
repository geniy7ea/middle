﻿// Middle.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
#include <cmath>
#include <vector>



class Vector
{
public:

    Vector()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    Vector(float x, float y, float z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    operator int()
    {
        return 0;
    }

    operator float()
    {
        return sqrt(x * x + y * y + z * z);
    }

    friend Vector operator+(const Vector& a, const Vector& b);
    
    friend std::ostream& operator<<(std::ostream& out, const Vector& v);

    friend bool operator>(const Vector& a, const Vector& b);

    friend Vector operator*(const Vector& v1, const int i);

    friend Vector operator-(const Vector& v2, const Vector& v3);

    friend std::istream& operator>>(std::istream& in, Vector &v);

    float operator[](int index)
    {
        switch (index)
        {
        case 0:
            return x;
            break;
        case 1:
            return y;
            break;
        case 2:
            return z;
            break;
        default:
            std::cout << "index error";
            return 0;
            break;
        }
    }

private:
    float x;
    float y;
    float z;
    int v;
};

Vector operator-(const Vector& v2, const Vector& v3)
{
    return Vector(v2.x - v3.x, v2.y - v3.y, v2.z - v3.z);
}

Vector operator*(const Vector& v1, const int i)
{
    return Vector(v1.x*i, v1.y*i, v1.z*i);
}

bool operator>(const Vector& a, const Vector& b)
{
    return false;
}

Vector operator+(const Vector& a, const Vector& b)
{
    return Vector(a.x + b.x, a.y + b.y, a.z + b.z);
}

std::istream& operator>>(std::istream& in, Vector& v)
{
    in >> v.x;
    in >> v.y;
    in >> v.z;
    return in;
}

std::ostream& operator<<(std::ostream& out, const Vector& v)
{
    out << ' ' << v.x << ' ' << v.y << ' ' << v.z;
    return out;
}

int main()
{
// умножение вектора на число:
    Vector v(0, 1, 2);
    int i = 2;
    Vector v1;
    v1 = v * i;
        std::cout << v1 << '\n';

//оператор разности:
    Vector v2(2, 4, 5);
    Vector v3(1, 1, 1);
    Vector v4;
    v4 = v2 - v3;
        std::cout << v4 << '\n';

//оператор ввода для вектора:
        std::cout << "Enter a point: \n";
        std::cin >> v;

        std::cout << "You entered: " << v << '\n';
    

    /*Vector v1(0, 1, 2);
    Vector v2(3, 4, 5);
    Vector v3;
    v3 = v1 + v2;
    std::cout << v3 <<'\n';
    std::cout << "v3 length " << static_cast<float>(v3);*/
}
